
const NOT_ALLOWED_MESSAGE = 'You are not allowed to visit this website';
const CONFIRM_TEXT = 'Are you sure you want to continue?';
const NAME_QUESTION = 'What is your name?';
const AGE_QUESTION = 'How old are you?';

let name;
let age;

do {
    name = prompt(NAME_QUESTION, name ?? '');
    age = prompt(AGE_QUESTION, age ?? '');
} while (!name || age === '' || isNaN(age));

age = Number(age);

const WELCOME_TEXT = `Welcome, ${name}`;

if (age < 18) {
    alert(NOT_ALLOWED_MESSAGE);
} else if (age >= 18 && age <= 22) {
    const isConfirm = confirm(CONFIRM_TEXT);

    if (isConfirm) {
        alert(WELCOME_TEXT);
    } else {
        alert(NOT_ALLOWED_MESSAGE);
    }
} else {
    alert(WELCOME_TEXT);
}



